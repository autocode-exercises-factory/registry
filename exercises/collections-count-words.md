# Collections. Count Words.

| Repo     | Link                                                                        |
|----------|-----------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/upskill-java/pro-javase/collections-count-word           |
| Solution | https://gitlab.com/upskill-java/pro-javase/collections-count-words-solution |

