# Matrix Transposition

| Repo     | Link                                                                        |
|----------|-----------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-arrays/transpose-matrix          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-arrays/transpose-matrix |

# Matrix transposition

The purpose of this exercise is to train you to work with arrays.

Estimated workload of this exercise is _45 min_.

### Description
Please, proceed to `TransposeMatrix`
class an implement its method `multiply`.

It takes a rectangular integer array (matrix) as a parameter and returns it transposed.

Consider an integer matrix represented as a **rectangular array**.
The task is to **transpose** a given matrix over its main diagonal.
The **transposition** of a matrix over its main diagonal is simply a flipped version of the original matrix. 