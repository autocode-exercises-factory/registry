# Max Value in Sequence

| Repo     | Link                                                                             |
|----------|----------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/max-value-in-sequence          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/max-value-in-sequence |

# Find the maximum integer in a sequence

The purpose of this exercise is to train you to use simple loops and conditional statements.

Estimated workload of this exercise is _20 min_.

### Description

Please, proceed to FindMaxInSeq
and write a program that reads a sequence of integer values from standard output and finds the maximum value. You must
place your solution into the `max`  method to pass tests.

Details:

- You must read sequence values until the next one is `0`. Zero value means end of the input sequence.
- The sequence is guaranteed to contain at least one value.