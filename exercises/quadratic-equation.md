# Quadratic Equation

| Repo     | Link                                                                               |
|----------|------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/quadratic-equation               |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/quadratic-equation      |

# Quadratic Equation

The purpose of this exercise is to train you to design programs that need decision trees.

Estimated workload of this exercise is _30 min_.

### Description

Please, proceed to the QuadraticEquation
class and implement a program to solve quadratic equations.

For the given quadratic equation coefficients (**ax<sup>2</sup> + bx + c = 0**),
return one or two roots of the equation if there is any in the set of real numbers.

Input value is given via `System.in`. Output value must be printed to `System.out`.

Output format is:
* `x1 x2` (two roots in any order separated by space) if there are two roots,
* `x1` (just the value of the root) if there is the only root,
* `no roots` (just a string value "no roots") if there is no root.

The `a` parameter is guaranteed to be not zero.

Hint: [Quadratic formula reference](https://en.wikipedia.org/wiki/Quadratic_formula)