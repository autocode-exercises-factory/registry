# Array X-times Iterators

| Repo     | Link                                                                                            |
|----------|-------------------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-design-patterns/array-x-times-iterators.git          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-design-patterns/array-x-times-iterators.git |

# Array X-times Iterators

**Iterator** is a behavioral design pattern that lets you traverse elements of a collection without exposing its
underlying representation.

Implement `com.epam.rd.autocode.iterator.Iterators`
method:

- `intArrayXTimesIterator` - returns an Iterator that iterates over given array but returns each array element given amount times.