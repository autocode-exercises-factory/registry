# Scheduling Assistant

Purpose of this exercise is to train you to work with Java DateTime API (introduced in Java 8)

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/javase/scheduling-assistant |
| Solution | https://gitlab.com/efimchik-autocode-tasks/javase/scheduling-assistant-solution |

