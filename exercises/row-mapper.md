# Row Mapper

Purpose of this exercise is to train you to use JDBC Result Set interface in order to get data from relational databases. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/row-mapper |
| Solution | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/row-mapper-solution |

