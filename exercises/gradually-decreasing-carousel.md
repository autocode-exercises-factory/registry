# Gradually Decreasing Carousel

| Repo     | Link                                                                                   |
|----------|----------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/gradually-decreasing-carousel           |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/gradually-decreasing-carousel  |

# Gradually Decreasing Carousel

The purpose of this exercise is to train you to use extend classes with `extends` keyword.

Estimated workload of this exercise is _30 min_.

Note, that if you have not done the "Decrementing Carousel" exercise,
you have to implement `DecrementingCarousel` and `CarouselRun` classes.
Details are specified in `DC.md`.

### Description

In this exercise you need to extend `DecrementingCarousel`.
You need to implement `GraduallyDecreasingCarousel`.
This subclass must decrement elements by gradually increasing decrement.
When you need to decrement an element for the first time, decrease it by `1`.
Next time you need to decrement the same element, decrease it by `2`.
Next time decrease by `3`, then by `4` and so on.
Remember that you must not make process non-positive elements.