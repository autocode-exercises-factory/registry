# Decrementing Carousel With Limit

| Repo     | Link                                                                                      |
|----------|-------------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/decrementing-carousel-with-limit           |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/decrementing-carousel-with-limit  |

# Decrementing Carousel With Limit

The purpose of this exercise is to train you to use extend classes with `extends` keyword.

Estimated workload of this exercise is _30 min_.

Note, that if you have not done the "Decrementing Carousel" exercise,
you have to implement `DecrementingCarousel` and `CarouselRun` classes.
Details are specified in `DC.md`.

### Description

In this exercise you need to extend `DecrementingCarousel`.
You need to implement `DecrementingCarouselWithLimitedRun`.
This subclass must decrement elements as a usual DecrementingCarousel.
The difference is that this implementation must produce a carousel run,
which limits number of calls to the `next` method.
When the limit of calls reached carousel run must consider itself finished.