# Average

| Repo     | Link                                                                    |
|----------|-------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/average               |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/average      |

# Average

The purpose of this exercise is to train you to use simple loops and conditional statements.

Estimated workload of this exercise is _20 min_.

### Description

Please, proceed to Average class
and write a program that reads a sequence of integer values from standard output and finds the average value.

Details:

- You must read sequence values until the next one is `0`. Zero value means end of the input sequence.
- The sequence is guaranteed to contain at least one value.
- Average value is also an **integer**. Use **integer** operations.
