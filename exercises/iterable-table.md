# Iterable Table

| Repo     | Link                                                                                   |
|----------|----------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-design-patterns/iterable-table.git          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-design-patterns/iterable-table.git |

# Iterable Table

## Description
**Iterable** is an object that may produce an Iterator. **Iterator** is a behavioral design pattern that lets you traverse elements of a collection without exposing its underlying representation.

Implement `com.epam.rd.autocode.iterator.Iterators` method:
- `table` - returns an Iterable that allows iteration over cells of a table. Column by column.\
  Ranges of a table is given as two arrays - string columns and int rows.\
  The result iterates over pairs of given columns and rows.\
  Such pair is a concatenation of a column mark and a row mark: column "A" and row `1` would give a result "A1".