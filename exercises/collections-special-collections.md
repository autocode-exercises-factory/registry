# Collections. Special Collections.
This task is about implementing certain requirements while you are restricted by Java Collections Framework interfaces.
You will learn how to tailor a specific data structure to some of Collections interface and will gain more understanding and awareness of methods provided by these interfaces.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/special-collections <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/special-collections |
| Solution | https://gitlab.com/upskill-java/pro-javase/special-collections-solution |

