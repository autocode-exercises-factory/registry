# Concurrent Wallet

Purpose of this exercise is to train you working Synchronizers of java.util.concurrent to create thread-safe solutions.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/wallet <br/> https://gitlab.com/payment-autocode-task/wallet-task-skeleton |
| Solution | https://gitlab.com/upskill-java/pro-javase/wallet-solution <br/> https://gitlab.com/payment-autocode-task/wallet-task-solution |
