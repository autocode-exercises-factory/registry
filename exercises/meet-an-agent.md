# Meet an Agent

| Repo     | Link                                                                     |
|----------|--------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/meet-an-agent          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/meet-an-agent |

# Meet an Agent

The purpose of this exercise is to familiarize you with simple conditional statements.

Estimated workload of this exercise is _20 min_.

### Description
Please, proceed to MeetAnAgent class
and write a program that:
- asks for an input number,
- if the input equals to the secret password number, prints "Hello, Agent",
- otherwise, prints "Access denied".

Secret password is stored in `final static int PASSWORD`.

It is guaranteed that the input is not null.