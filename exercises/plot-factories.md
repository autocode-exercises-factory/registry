# Plot Factories

| Repo     | Link                                                                                   |
|----------|----------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-design-patterns/plot-factories.git          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-design-patterns/plot-factories.git |

# Plot Factories
**Abstract Factory** is a creational design pattern that lets you produce families of related objects without specifying their concrete classes.

Implement `com.epam.rd.autocode.factory.plot.PlotFactories` methods:
- `classicDisneyPlotFactory` - returns a factory that creates a plot for a classic Disney animated film.
- `contemporaryDisneyPlotFactory` - returns a factory that creates a plot for a contemporary Disney animated film.
- `marvelPlotFactory` - returns a factory that creates a plot for a Marvel movie.

API Details:
- `Character` interface.\
  Represents a plot character. Has the `name()` method, that returns its name.
- `EpicCrisis` interface.\
  Represents an epic crisis. Has the `name()` method, that returns its name.
- `Plot` interface.\
  Represents a plot. Has `asText()` method, that returns the text of the plot.
- `PlotFactory` interface.\
  Represents a plot factory. Produces plots via the `plot()` method.