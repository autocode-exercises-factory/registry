# Thread Factoring

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/thread-factoring <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/thread-factoring |
| Solution | https://gitlab.com/upskill-java/pro-javase/thread-factoring-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/thread-factoring-solution |

