# Beware of Dogs

| Repo     | Link                                                                        |
|----------|-----------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-generics/beware-of-dogs          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-generics/beware-of-dogs |

# Beware of dogs

The purpose of this exercise is to train you to work with generics.

Estimated workload of this exercise is _30 min_.

### Description
Please, use generics to change `House` class in order to dogs cannot enter cats' house and cats cannot enter dogs' house.

You need to alter `residents` field and `enter` methods of the `House` class.

Note that if you have done everything right, then two particular lines in the `Main` class will prevent successful compilation, so you need to remove them.
See details in the `Main` class.
