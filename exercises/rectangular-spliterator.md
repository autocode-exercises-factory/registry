# Rectangular Spliterator

Purpose of this exercise is to train you to design custom spliterators.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/javase/rectangular-spliterator |
| Solution | https://gitlab.com/efimchik-autocode-tasks/javase/rectangular-spliterator-solution |

