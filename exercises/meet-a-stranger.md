# Meet a stranger

| Repo     | Link                                                                           |
|----------|--------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/meet-a-stranger              |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/meet-a-stranger.git |

# Meet a stranger

The purpose of this exercise is to familiarize you with basic usage of standard input stream.

Estimated workload of this exercise is _5 minutes_.

### Description
Please, proceed to the class MeetAStranger.
The program must read a string from `System.in` and print a message "Hello, *input*".

Note that when entering an input string consisting of several words, the entire input must be printed.