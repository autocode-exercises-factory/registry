# Triangle

| Repo     | Link                                                                      |
|----------|---------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-oop/triangle                   |
| Solution | https://gitlab.com/autocode-exercises-solution/java-oop/triangle          |

# Triangle

The purpose of this exercise is to train you to work with classes and methods.

Estimated workload of this exercise is _45 min_.

### Description

Please, implement methods of class `Triangle`:

* constructor, which has three points as parameters.\
  Make sure that these points refer to vertices of the triangle.\
  Ensure that the created triangle exists and it is not degenerative.\
  If it is, use `throw new IllegalArgumentException()` to raise an error.
* `double area()`\
  Return the area of the triangle.
* `Point centroid()`\
  Return the centroid of the triangle.
