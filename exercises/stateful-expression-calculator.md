# Stateful Expression Calculator

Purpose of this exercise is to train you to handle session state working with Servlets

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/servlets/stateful-expression-calculator |
| Solution | https://gitlab.com/efimchik-autocode-tasks/servlets/stateful-expression-calculator-solution |

