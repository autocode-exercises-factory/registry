# Spring MVC Stateful Expression Calculator

Purpose of this exercise is to train you to create stateful web applications using Spring MVC 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/spring-mvc/spring-mvc-stateful-expression-calculator |
| Skeleton | https://gitlab.com/upskill-java/spring/spring-mvc-stateful-expression-calculator |
| Solution | https://gitlab.com/efimchik-autocode-tasks/spring-mvc/stateful-expression-calculator-solution |
| Solution | https://gitlab.com/upskill-java/spring/stateful-expression-calculator-solution |

