# Hashtable Open 8-16
Purpose of this exercise is to train you to work with advanced data structures and Hashtable in particular

| Repo     | Link                                                                                                                                                                |
|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/upskill-java/pro-javase/hashtable-open-8-16 <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/hashtable-open-8-16                   |
| Solution | https://gitlab.com/upskill-java/pro-javase/hashtable-open-8-16-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/hashtable-open-8-16-solution |

