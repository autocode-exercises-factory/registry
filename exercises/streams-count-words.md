# Streams. Count Words.
Purpose of this exercise is to train you working with Stream API and lambdas.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/pro-javase/streams-count-words <br/> https://gitlab.com/efimchik-autocode-tasks/java-extended/streams-count-words |
| Solution | https://gitlab.com/upskill-java/pro-javase/streams-count-words-solution |

