# Set Mapper

Purpose of this exercise is to train you to use JDBC Result Set interface advanced techniques. 

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/set-mapper |
| Solution | https://gitlab.com/efimchik-autocode-tasks/sql-jdbc/set-mapper-solution |

