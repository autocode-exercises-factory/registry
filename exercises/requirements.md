# Requirements

| Repo     | Link                                                                        |
|----------|-----------------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-exceptions/requirements          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-exceptions/requirements |

# Requirements

The purpose of this exercise is to train you to work with exceptions, to raise them in particular.

Estimated workload of this exercise is _30 min_.

### Description
Please, implement Requirements methods:
1. `requireNonNull(Object)` should throw new NullPointerException if object is null
1. `requireNonNull(Object, String)` should throw new NullPointerException with message if object is null
1. `checkArgument(boolean)` if boolean is false should throw new IllegalArgumentException
1. `checkArgument(boolean, String)` if boolean is false should throw new IllegalArgumentException with message
1. `checkState(boolean)` if boolean is false should throw new IllegalStateException
1. `checkState(boolean, String)` if boolean is false should throw new IllegalStateException with message
1. `checkElementIndex(int, int)` if index out of bounds throw new IndexOutOfBoundsException

Such methods might be helpful to check arguments, object states. 