# Suffixing App

Purpose of this exercise is train you to design and build a Java application all by yourself, basing on high-level
requirements. You will have to handle command-line arguments, work with files and use logging.

| Repo | Link |
| --- | --- |
| Skeleton | https://gitlab.com/upskill-java/deep-dive/suffixing-app <br/> https://gitlab.com/efimchik-autocode-tasks/java-basics/suffixing-app |
| Solution | https://gitlab.com/upskill-java/deep-dive/suffixing-app-solution <br/> https://gitlab.com/efimchik-autocode-tasks/java-basics/suffixing-app-solution |
| Reference App Implementation | https://gitlab.com/upskill-java/deep-dive/suffixing-app-reference-impl <br/> https://gitlab.com/efimchik-autocode-tasks/java-basics/suffixing-app-reference-impl |

