# Snail

| Repo     | Link                                                             |
|----------|------------------------------------------------------------------|
| Skeleton | https://gitlab.com/autocode-exercises/java-basics/snail          |
| Solution | https://gitlab.com/autocode-exercises-solution/java-basics/snail |

# Snail
The purpose of this exercise is to train you in usage of simple integer operations.

Estimated workload of this exercise is _30 min_.

### Description
Consider a snail travels up a tree `a` feet each day.Then snail slides down `b` feet each night. Height of the tree is `h`.

Please, proceed to Snail class
and write a program that prints number of days for the snail to reach the top of the tree.

Program reads `a`, `b`, `h` line by line. Input values are guaranteed to be positive integers.

If the snail cannot reach the top of the tree, print the message `Impossible`.