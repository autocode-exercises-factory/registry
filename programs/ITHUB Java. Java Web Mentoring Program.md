# ITHUB Java. Java Web Mentoring Program

## Exercises

- Servlets
    - [Expression Calculator](../exercises/expression-calculator.md)
    - [Stateful Expression Calculator](../exercises/stateful-expression-calculator.md)
- JSP
    - [JSP Currencies](../exercises/jsp-currencies.md)