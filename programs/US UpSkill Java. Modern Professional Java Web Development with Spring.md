# Modern Professional Java Web Development with Spring

## Exercises

- Spring Core
  - [Spring Chess Puzzles](../exercises/spring-chess-puzzles.md)
  - [Spring Chess Puzzles XML](../exercises/spring-chess-puzzles-xml.md)
  - [Spring Video Beans](../exercises/spring-video-beans.md)
  - [Spring Custom Scopes](../exercises/spring-custom-scopes.md)
- Spring MVC
  - [Stateful Expression Calculator (Spring)](../exercises/spring-mvc-stateful-expression-calculator.md)
- Spring MVC
  - [Employee Catalog](../exercises/employee-catalog.md)
- Spring Security
  - [Spring Catalog Access](../exercises/spring-catalog-access.md)