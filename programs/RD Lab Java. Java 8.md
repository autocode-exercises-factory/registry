# RD Lab Java. Java 8

## Exercises

- Meet Autocode
    - [Meet Autocode](../exercises/meet-autocode.md)
- Streams
    - [Streams. Count Words.](../exercises/streams-count-words.md)
    - [Streams. Pipelines.](../exercises/streams-pipelines.md)
- Spliterators
    - [Rectangular Spliterator](../exercises/rectangular-spliterator.md)
- DateTime API
    - [Scheduling Assistant](../exercises/scheduling-assistant.md)
    
