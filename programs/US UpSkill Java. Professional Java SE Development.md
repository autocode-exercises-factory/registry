# Professional Java SE Development

## Exercises

- Object-Oriented Software Design And Programming
  - [Design Patterns](../exercises/design-patterns.md)
- Algorithms And Data Structures
  - [Flood Fill](../exercises/flood-fill.md)
  - [BST Pretty Print](../exercises/bst-pretty-print.md)
  - [Hashtable Open 8-16](../exercises/hashtable-open-8-16.md)
- Java Generics And Collections
  - [Collections. Count Words.](../exercises/collections-count-words.md)
  - [Collections. Special Collections.](../exercises/collections-special-collections.md)
- Java Lambdas & Streams
  - [Streams. Count Words.](../exercises/streams-count-words.md)
  - [Streams. Pipelines.](../exercises/streams-pipelines.md)
- Java I/O
  - [File Tree](../exercises/file-tree.md)
- Java Concurrency Essentials
  - [Thread Factoring](../exercises/thread-factoring.md)
  - [Concurrent TicTacToe](../exercises/concurrent-tictactoe.md)
  - [Wallet](../exercises/concurrent-wallet.md)
