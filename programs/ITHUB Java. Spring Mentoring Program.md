# ITHUB Java. Spring Mentoring Program

## Exercises

- Spring Core
    - [Spring Chess Layouts](../exercises/spring-chess-layouts.md)
    - [Spring Video Beans](../exercises/spring-video-beans.md)
- Spring MVC
    - [Spring MVC Stateful Expression Calculator](../exercises/spring-mvc-stateful-expression-calculator.md)
- Spring Boot
    - [Employee Catalog](../exercises/employee-catalog.md)
- Spring Security
    - [Spring Catalog Access](../exercises/spring-catalog-access.md)