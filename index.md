# Index

1. [arithmetic-expressions](exercises/arithmetic-expressions.md)
2. [array-x-times-iterators](exercises/array-x-times-iterators.md)
3. [average](exercises/average.md)
4. [battleship8x8](exercises/battleship8x8.md)
5. [beware-of-dogs](exercises/beware-of-dogs.md)
6. [bst-pretty-print](exercises/bst-pretty-print.md)
7. [card-dealing-strategies](exercises/card-dealing-strategies.md)
8. [catch-em-all](exercises/catch-em-all.md)
9. [collections-count-words](exercises/collections-count-words.md)
10. [collections-special-collections](exercises/collections-special-collections.md)
11. [compass](exercises/compass.md)
12. [concurrent-tictactoe](exercises/concurrent-tictactoe.md)
13. [concurrent-wallet](exercises/concurrent-wallet.md)
14. [contact-book](exercises/contact-book.md)
15. [cycle-swap](exercises/cycle-swap.md)
16. [dao](exercises/dao.md)
17. [decrementing-carousel](exercises/decrementing-carousel.md)
18. [decrementing-carousel-with-limit](exercises/decrementing-carousel-with-limit.md)
19. [design-patterns](exercises/design-patterns.md)
20. [electronic-watch](exercises/electronic-watch.md)
21. [employee-catalog](exercises/employee-catalog.md)
22. [even-indices-elements-sublist-decorator](exercises/even-indices-elements-sublist-decorator.md)
23. [expression-calculator](exercises/expression-calculator.md)
24. [figures](exercises/figures.md)
25. [figures-extra-challenge](exercises/figures-extra-challenge.md)
26. [file-tree](exercises/file-tree.md)
27. [flood-fill](exercises/flood-fill.md)
28. [go-dutch](exercises/go-dutch.md)
29. [gradually-decreasing-carousel](exercises/gradually-decreasing-carousel.md)
30. [halving-carousel](exercises/halving-carousel.md)
31. [hashtable-open-8-16](exercises/hashtable-open-8-16.md)
32. [int-range-set](exercises/int-range-set.md)
33. [int-string-capped-map](exercises/int-string-capped-map.md)
34. [iterable-table](exercises/iterable-table.md)
35. [jsp-currencies](exercises/jsp-currencies.md)
36. [line-intersection](exercises/line-intersection.md)
37. [local-maxima-remove](exercises/local-maxima-remove.md)
38. [matrices-multiplication](exercises/matrices-multiplication.md)
39. [matrix-transposition](exercises/matrix-transposition.md)
40. [max-method](exercises/max-method.md)
41. [max-value-in-sequence](exercises/max-value-in-sequence.md)
42. [median-queue](exercises/median-queue.md)
43. [meet-a-stranger](exercises/meet-a-stranger.md)
44. [meet-an-agent](exercises/meet-an-agent.md)
45. [meet-autocode](exercises/meet-autocode.md)
46. [meet-strangers](exercises/meet-strangers.md)
47. [optional-max](exercises/optional-max.md)
48. [pizza-split](exercises/pizza-split.md)
49. [plot-factories](exercises/plot-factories.md)
50. [quadratic-equation](exercises/quadratic-equation.md)
51. [ranged-ops-integer-set](exercises/ranged-ops-integer-set.md)
52. [rectangular-spliterator](exercises/rectangular-spliterator.md)
53. [repository-observer](exercises/repository-observer.md)
54. [requirements](exercises/requirements.md)
55. [row-mapper](exercises/row-mapper.md)
56. [scheduling-assistant](exercises/scheduling-assistant.md)
57. [segments](exercises/segments.md)
58. [service](exercises/service.md)
59. [set-mapper](exercises/set-mapper.md)
60. [snail](exercises/snail.md)
61. [spiral](exercises/spiral.md)
62. [spring-catalog-access](exercises/spring-catalog-access.md)
63. [spring-chess-layouts](exercises/spring-chess-layouts.md)
64. [spring-chess-puzzles](exercises/spring-chess-puzzles.md)
65. [spring-chess-puzzles-xml](exercises/spring-chess-puzzles-xml.md)
66. [spring-custom-scopes](exercises/spring-custom-scopes.md)
67. [spring-mvc-stateful-expression-calculator](exercises/spring-mvc-stateful-expression-calculator.md)
68. [spring-video-beans](exercises/spring-video-beans.md)
69. [sprint-planning](exercises/sprint-planning.md)
70. [sql-queries](exercises/sql-queries.md)
71. [stateful-expression-calculator](exercises/stateful-expression-calculator.md)
72. [streams-count-words](exercises/streams-count-words.md)
73. [streams-pipelines](exercises/streams-pipelines.md)
74. [suffixing-app](exercises/suffixing-app.md)
75. [sum-of-even-numbers](exercises/sum-of-even-numbers.md)
76. [sum-of-previous](exercises/sum-of-previous.md)
77. [task-carousel](exercises/task-carousel.md)
78. [test-factorial](exercises/test-factorial.md)
79. [test-quadratic-equation](exercises/test-quadratic-equation.md)
80. [test-sorting](exercises/test-sorting.md)
81. [thread-factoring](exercises/thread-factoring.md)
82. [triangle](exercises/triangle.md)
83. [validations-color-code](exercises/validations-color-code.md)
84. [validations-epam-email](exercises/validations-epam-email.md)
85. [words](exercises/words.md)