### Prerequisites
- https://autocode-next.lab.epam.com/help \
  Guide to students and contributors. Open.\
  Covering:
  - registration, 
  - Gitlab integration,
  - subscribing to a course,
  - submitting solutions.
  - when having contributor access:
    - Managing courses
    - Course structure
    - Course contributors
    - Course students
- https://videoportal.epam.com/video/Qa1m1Xak \
  Recorded Autocode Demo (EPAM only, Russian)

Make sure to study them first.

### Exercise Development
The following guidelines describe technical advices on how to develop an exercise.
- Pre-Development Ticket Activity
  - Create or get an exercise development task from Autocode Exercises Factory issue board: https://gitlab.com/autocode-exercises-factory/registry/-/boards/2662674.
    Assign yourself to an exercise development ticket or ask @efimchik_epam to do it.
    Move the ticket to `In Progress` column.
- Exercise Development
  - Develop a skeleton code.
  - Cover it with tests.
  - Implement a solution.
  - Ensure that the solution passes tests.
  - Ensure that README.md contains a thorough description of the exercise, solution requirements and success criteria.
  - Only when everything is ready, start committing. Initialize local git repository.
  - Submit skeleton code to `master` branch.
    Make sure to design the skeleton code distinguishable from solution code: use factory methods, interfaces, etc.
    Be sure to avoid submitting solution details to `master` branch. 
  - Fork `solution` branch from `master` branch and submit solution code.
    Considering that you have designed skeleton code with care, such commits will mostly add files rather than modify them.
  - Optionally, you may consider use tests, that are hidden from students.\
    In this case you need to create one branch `tests`
- Gitlab Repositories
  - Create a skeleton Gitlab repository in a private group.
    You may use subgroups of Autocode Exercises Factory group, your own groups, or some other groups if approved with exercise stakeholders. 
    Name the repository after the exercise, using kebab-case. E.g., `my-awesome-exercise`.
  - Create another Gitlab repository for the solution.
    Name it the same way, but add `-solution` suffix. E.g., `my-awesome-exercise-solution`.
  - Add these two Gitlab repositories as remotes to your local exercise repo, like:
    ```git
    git remote add skeleton *skeleton Gitlab repo URL*
    git remote add solution *skeleton Gitlab repo URL*
    ```
  - Push local `master` branch to `master` branch of *skeleton* remote repository.
  - Push local `solution` branch to `solution` branch of *solution* remote repository.
  - If you are adding a special tests repository, create one more repository, with `-tests` suffix\
    This repository must contain *all the tests*, not only hidden ones.
- AutoCode Task
  - Create a task via AutoCode interface, specify skeleton repository.
  - If you have created a repository with hidden tests, specify it as internal tests repo.\
    If you have no separate tests repo, you may specify skeleton repo as internal tests repo as well, though, it is not mandatory. 
  - Check the exercise via `Sandbox` interface.\
    Specify *solution* repository link as `Task repository`.\
    Specify *skeleton* (or *tests*) repository  link as `Tests repository`.\
    Specify branch names accordingly.\
    Hit run button and make sure all the steps go green.\
    Make sure that all the tests passed - review unit tests step details.\
    Make sure SonarQube has not found issues (if it is relevant to the exercise).
- Post-Development Ticket Activity
  - Submit the exercise ticket to `On Review` column in Autocode Exercises Factory issue board: https://gitlab.com/autocode-exercises-factory/registry/-/boards/2662674.
    Consider assigning th ticket to an SME. For instance, @efimchik_epam.


  

  
  
  

